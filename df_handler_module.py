import pandas as pd
import SQL
import json


#считываем настройки
def read_params(fn): 
    d ={} 
    try:
        with open(fn, 'r',encoding="utf-8") as file: 
            d = json.load(file) 
    except FileNotFoundError:
        print ("Error. Can't find file " + fn)
        d = {}
    return d 



class DataFrameHandler():
    #read pandas from csv
    def __init__(self,
                 name2file = None,
                 sql_list = None
                 ):
        self.result_dataset_total = pd.DataFrame()
        self.data_frames_dict = {}
        self.load_data_from_sql(sql_list)
   
    def load_data_from_csv(self, name2file):
        for name in name2file:    
            print('read dataframe %s from csv file' %name)
            self.data_frames_dict[name] = pd.read_csv(name2file[name], sep=',', decimal=',')
   
    def load_data_from_sql(self, sql_list):
        settings = read_params("settings.json")
        source2server = {0: "sql_stat", 1: "sql_statdc", 2:"sql_stat3", 3:"sql_falcon"}
        global sql_source, sql_dest, rows
        for name in sql_list:
            sql_source = SQL.operationsSQL(settings[source2server [sql_list[name]["src_server"]]])
            print('read dataframe %s from % sql table' % (name,sql_list[name]["src_table"]) )    
            self.data_frames_dict[name] = sql_source.getDataFrameFromQueryResNoHead(sql_list[name]["src_table"], None, None)
           
    
    def merge_multiple(self, drop_columns, type_merge, merge_columns):
        self.result_dataset_total = self.data_frames_dict['sessions_by_day']
        for name in self.data_frames_dict:
            for drop_cl in drop_columns:
                    if  drop_cl in self.result_dataset_total.columns:
                        self.result_dataset_total  = self.result_dataset_total.drop(columns=drop_cl)
            if name == 'sessions_by_day':
                continue;
            if not all(x in self.data_frames_dict[name].columns for x in merge_columns):
                continue
            if not all(x in self.result_dataset_total.columns for x in self.data_frames_dict[name].columns):      
                self.result_dataset_total = pd.merge(self.result_dataset_total,
                                    self.data_frames_dict[name],
                                    on = merge_columns,
                                    how = type_merge)
            
            
        self.result_dataset_total = pd.merge(self.result_dataset_total,
                                self.data_frames_dict['regs_total'],
                                on = ['RegID'],
                                how = "inner")
                                             
        for drop_cl in drop_columns:
            if  drop_cl in self.result_dataset_total.columns:
                self.result_dataset_total  = self.result_dataset_total.drop(columns=drop_cl)
                                             
    def delete_columns(self, drop_columns):  
        for drop_cl in drop_columns:
            if  drop_cl in self.result_dataset_total.columns:
                self.result_dataset_total  = self.result_dataset_total.drop(columns=drop_cl)
                
        
    def get_dataframe_by_name(self, name):
        return self.data_frames_dict[name]
   
    def rename_columns(self, columns_rename, type = 0):
        if type == 0:
            for name in self.data_frames_dict:
                for column in columns_rename:
                    if column in self.data_frames_dict[name].columns:
                        self.data_frames_dict[name] = self.data_frames_dict[name].rename(index = str, columns = {column: columns_rename[column]}) 
        else:
            for column in columns_rename:
                self.result_dataset_total = self.result_dataset_total.rename(index = str, columns = {column: columns_rename[column]}) 
        for column in  self.result_dataset_total.columns:
            self.result_dataset_total = self.result_dataset_total.rename(index = str, columns = {column: column.lower()}) 
                
    
    def set_date_diff(self, column_from, column_to):
        for name in self.data_frames_dict:
            if column_from in self.data_frames_dict[name].columns and  column_to in self.data_frames_dict[name].columns: 
                self.data_frames_dict[name]['Day_Diff'] = -(self.data_frames_dict[name][column_from] - self.data_frames_dict[name][column_to]).dt.days
    
    def convert_columns(self, conver_dic):
        #cycle by dataframes
        for name in self.data_frames_dict:
            for r in zip(self.data_frames_dict[name].columns, self.data_frames_dict[name].dtypes):
                if (r[1] == 'bool'):
                    if (str(self.data_frames_dict[name][r[0]].dtypes).find('int') < 0 and  str(self.data_frames_dict[name][r[0]].dtypes).find('float') < 0):
                        col = str(r[0])
                        print('convert column %s dataframe %s from %s to %s' % (r[0], name, 'bool', 'int'))
                        self.data_frames_dict[name][col]  = self.data_frames_dict[name][col].astype(int)    
                     
            
            for type in conver_dic:
                if type == 'datetime':
                    for col in conver_dic[type]:
                        if col in self.data_frames_dict[name].columns:
                            if  str(self.data_frames_dict[name][col].dtypes).find('datetime') < 0:
                                print('convert column %s dataframe %s from %s to %s' % (col, name, str(self.data_frames_dict[name][col].dtypes), 'datetime'))
                                self.data_frames_dict[name][col] = self.data_frames_dict[name][col].apply(pd.to_datetime)
                      
                if type == 'numeric':
                    for col in conver_dic[type]:
                        if col in self.data_frames_dict[name].columns:
                            if (str(self.data_frames_dict[name][col].dtypes).find('int') < 0 and  str(self.data_frames_dict[name][col].dtypes).find('float') < 0):
                                print('convert column %s dataframe %s from %s to %s' % (col, name, str(self.data_frames_dict[name][col].dtypes), 'numeric'))
                                self.data_frames_dict[name][col]  = self.data_frames_dict[name][col].apply(pd.to_numeric)  
                            
                        
    def merge_add_from(self, type_merge, merge_columns, add_columns, key_merge):
        for name in self.data_frames_dict:
            if name == key_merge:
                continue;
            if not all(x in self.data_frames_dict[name].columns for x in add_columns):     
                self.data_frames_dict[name] = pd.merge(self.data_frames_dict[name],
                                   self.data_frames_dict[key_merge][add_columns],
                                   on = merge_columns,
                                   how = type_merge) 
    
    