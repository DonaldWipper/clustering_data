import pandas as pd
from sklearn import datasets
import seaborn as sns
import numpy as np
from pylab import rcParams
rcParams['figure.figsize'] = 8, 5
from sklearn.cluster import DBSCAN, KMeans
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import Normalizer
from sklearn import preprocessing
import os
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA


class clustering():
    def __init__(self, 
                 cur_day,
                 type_clustering,
                 type_dim_reduction, #0 - tnse 
                 count_clusters,
                 input_dataframe,
                 type_normalize, #тип нормализации.
                 data_reg = '',
                 rpid = 3
                 ):
        self.type_clustering = type_clustering
        self.count_clusters = count_clusters
        self.type_dim_reduction = type_dim_reduction
        self.input_dataframe = input_dataframe
        self.cur_day = cur_day
        self.normalize_df = None  #normalize data
        self.clusters_df = None   #clusters
        self.union_df = None      #data + clusters
        self.centroids = None
        self.uniq_values = None   #count distinct values in columns 
        self.output_dataframe = self.input_dataframe 
        self.type_normalize = type_normalize
        self.data_reg = data_reg
        self.rpid = rpid
    
    #построение кластеризации
    def start_clustering(self, del_columns, column_levels):
        self.drop_non_feat_columns(del_columns)
        self.fields_string2number()
        self.normalize_data()
        self.get_clusters()
        self.make_dim_reduction()
        self.get_uniq_values()
        self.visualize_data(column_levels)
    
    #получение оптимального числа кдастеров
    def get_cluster_number(self, del_columns, column_levels):
        self.drop_non_feat_columns(del_columns)
        self.fields_string2number()
        self.normalize_data()
        self.__get_optimal_number_clusters()
    
    #transform non numerical fields    
    def fields_string2number(self):
        self.input_dataframe = self.input_dataframe.replace(np.NaN, 0) 
        self.output_dataframe = self.output_dataframe.replace(np.NaN, 0) 
        transform_columns = list(self.output_dataframe.select_dtypes(include=['bool']).columns) + list(self.output_dataframe.select_dtypes(include=['object']).columns)
        for cat in transform_columns:
            if self.output_dataframe[cat].dtypes == 'object':
                self.output_dataframe[cat] = self.output_dataframe[cat].astype('str') 
            le = LabelEncoder()
            le.fit(self.output_dataframe[cat])
            self.output_dataframe[cat] = le.transform(self.output_dataframe[cat])    

    def drop_non_feat_columns(self, del_columns):
        for d in del_columns:
            if d in self.output_dataframe.columns:
                self.output_dataframe = self.output_dataframe.drop(columns = d)  
        
    def normalize_data(self):
        if self.type_normalize == 0:
            norm_df = Normalizer().fit_transform(self.output_dataframe)
     
        elif self.type_normalize == 1:
            scaler = MinMaxScaler()
            norm_df = scaler.fit_transform(self.output_dataframe)
        
        elif self.type_normalize == 2:
            norm_df = preprocessing.scale(self.output_dataframe)   
          
        elif self.type_normalize == 3:
            scaler = StandardScaler()
            norm_df = scaler.fit_transform(self.output_dataframe)
            
        self.normalize_df = pd.DataFrame(norm_df, columns = self.output_dataframe.columns)    
       
    def __get_dim_normalize_name(self,number):
        if self.type_normalize == 0:
            return 'normalize'
        elif self.type_normalize == 1:
            return 'min_max_scaler'
        elif self.type_normalize == 2:
            return 'scale'
        elif self.type_normalize == 3:
            return 'standart_scaler' 
    
    def get_image_path(self, section = ''):
        path_list = []
        method = self.__get_dim_reduction_name(self.type_dim_reduction)  
        normalize = self.__get_dim_normalize_name(self.type_normalize)
        clusters = str(self.count_clusters).zfill(2) + "_clusters"  
        path_list.append(method)
        path_list.append(normalize)
        path_list.append(clusters)
        path_list.append(str(self.rpid) + '_RPID')
        path_list.append(str(self.data_reg) + 'REGDATE')
        path_list.append(section)
        path = ''
        for p in path_list:
            if path == '':
                path = p
            else:    
                path = path + '/' + p
            if not os.path.exists(path):
                os.makedirs(path)    
        return path

        
    def get_clusters(self):
        if self.type_clustering == 0:
            kmeans = KMeans(n_clusters = self.count_clusters).fit(self.normalize_df)
            centroids = kmeans.cluster_centers_
            self.centroids = pd.DataFrame(centroids, columns = self.normalize_df.columns)
            self.input_dataframe["cluster_number"] = kmeans.labels_
            print(self.input_dataframe[['regid', "cluster_number"]].groupby("cluster_number").count().transpose())
            
    def get_uniq_values(self):
        counts = self.input_dataframe.nunique()
        self.uniq_values = dict(zip(self.input_dataframe.columns, counts))
     
    def __get_dim_reduction_name(self,number):
        if self.type_dim_reduction == 0:
            return 'tsne'
        if self.type_dim_reduction == 1:
            return 'pca'
        
    def __get_optimal_number_clusters(self):
        Sum_of_squared_distances = []
        abs_diff = []
        prev = 1
        cur  = 0
        K = range(1,20)
        print('calculate optimal cluster number ' + self.get_image_path())
        for k in K:
            #print('count clusters %d' % k)
            kmeans = KMeans(n_clusters = k).fit(self.normalize_df)
            Sum_of_squared_distances.append(kmeans.inertia_)
            cur = kmeans.inertia_
            if (prev == 1):
                abs_diff.append(1)
                prev = cur
            else:
                abs_diff.append(abs(cur - prev) * 1.0 / prev)
                prev = cur
        plt.grid()
        plt.plot(K, Sum_of_squared_distances, 'bx-')
        plt.xlabel('k')
        plt.ylabel('Sum_of_squared_distances')
        plt.title('Elbow Method For Optimal k')
        for i in range(len(K)):
            plt.text(K[i], Sum_of_squared_distances[i], '{:.3%}'.format(float(abs_diff[i])) , color="green")
                
        plt.savefig(self.get_image_path(section = 'optimal_cluster/') + '%sDAY.png' % str(self.cur_day).zfill(2), fontsize = 6)   
        plt.clf()
                                   
    
    def make_dim_reduction(self):
        self.union_df  = pd.concat([self.normalize_df, self.centroids])  
        if self.type_dim_reduction == 0:
            tsne = TSNE(n_components=2, verbose = 1, metric='euclidean', perplexity=40, n_iter=250)
            df_tsne = pd.DataFrame(tsne.fit_transform(self.union_df))
            df_tsne.columns = ['x1', 'x2']
            self.clusters_df = df_tsne[len(df_tsne)-self.count_clusters:len(df_tsne)]
            self.clusters_df["cluster_number"] = [i for i in range(0, self.count_clusters)]
            df_tsne2 = df_tsne[0:len(df_tsne)-self.count_clusters]
            self.input_dataframe = self.input_dataframe.reset_index()
            self.input_dataframe["x1"] = df_tsne2["x1"]
            self.input_dataframe["x2"] = df_tsne2["x2"]
            #print(self.clusters_df)
            del df_tsne2 
            del df_tsne
            del self.output_dataframe
            #del self.normalize_df
            del self.centroids
            
        elif self.type_dim_reduction == 1: 
            pca = PCA(n_components=2)
            pca_result = pd.DataFrame(pca.fit_transform(self.union_df))
            pca_result.columns = ['x1', 'x2']
            self.clusters_df = pca_result[len(pca_result)-self.count_clusters:len(pca_result)]
            self.clusters_df["cluster_number"] = [i for i in range(0, self.count_clusters)]
            pca_result2 = pca_result [0:len(pca_result) - self.count_clusters]
            self.input_dataframe = self.input_dataframe.reset_index()
            self.input_dataframe["x1"] = pca_result2["x1"]
            self.input_dataframe["x2"] = pca_result2["x2"]
            del pca_result2 
            del pca_result
            del self.output_dataframe
            #del self.normalize_df
            del self.centroids
    
    def visualize_data(self, column_levels):
        #print(self.uniq_values)
        mine_df = self.input_dataframe[self.input_dataframe["regid"] == 849259411]
        plt.figure(figsize=(10,10))
        for field in self.uniq_values:
            if field not in column_levels:
                continue;
            
            if self.uniq_values[field] > 20:
                sns.scatterplot(
                    x="x1", y="x2",
                    hue=field,
                    palette=sns.color_palette("husl", n_colors = self.uniq_values[field]),
                    data = self.input_dataframe,
                    alpha=0.6
                )
                
            else:
                sns.scatterplot(
                    x="x1", y="x2",
                    hue=field,
                    palette=sns.color_palette("husl", n_colors = self.uniq_values[field]),
                    data = self.input_dataframe,
                    legend="full",
                    alpha=0.6
                )
            sns.scatterplot(
                x="x1", y="x2",
                data = self.clusters_df,
                legend="full",
                marker="x",
                color = "black"
                )
            sns.scatterplot(
                x="x1", y="x2",
                data = mine_df,
                legend="full",
                marker="x",
                color = "green"
            )
            
            
         
     
            for cluster in zip(self.clusters_df["x1"].values, self.clusters_df["x2"].values, self.clusters_df["cluster_number"].values):
                plt.text(cluster[0], cluster[1], str(cluster[2]), color="red", fontsize = 50) 
    
            for cluster in zip(mine_df["x1"].values, mine_df["x2"].values):
                plt.text(cluster[0], cluster[1], "ME", color="green", fontsize = 50)
            
            plt.grid()
            #print(self.get_image_path(section = field) + '/' + '%sDAY.png' % str(self.cur_day).zfill(2)) 
            plt.savefig(self.get_image_path(section = field) + '/' + '%sDAY.png' % str(self.cur_day).zfill(2))  
            plt.clf()
            
        